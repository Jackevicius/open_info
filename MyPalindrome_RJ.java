
//import java.util.stream.IntStream; 

//import java.text.SimpleDateFormat;
import java.time.LocalDate;
//import java.util.Calendar;
//import java.util.Date;
//import java.util.GregorianCalendar;


public class MyPalindrome_RJ {
  static void printBonusDatesBetween(int from_year, int to_year) {
	  
	  // if input isn't correct - from_year > to_year swap years
	  if (from_year > to_year) {
		  int temp_year;
		  temp_year = from_year;
		  from_year = to_year;
		  to_year = temp_year;
		  System.out.println("input years are swapped, because your input isn't: [from_year <= to_year)");
	  }	  
	  
	  if (from_year < 1000 || to_year > 10000) {
		  System.out.println("Out of range!, you can choose years in interval: [1000:10000]");
		  System.out.println("You are choosed:");
		  System.out.println("-from_year: " + from_year);
		  System.out.println("-to_year: " + to_year);

	  }
	  else{
		  		  
		  // jan, feb, mar, ...
		  int[] max_days_in_month = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
		  

			for(int present_year = from_year; present_year < to_year; present_year++){
		
				int year = present_year;
				int reversed_year = 0;
		
				while(year != 0) {
		            int digit = year % 10;
		            reversed_year = reversed_year * 10 + digit;
		            year /= 10;
		        }
				

				int month = reversed_year / 100;
				int day = reversed_year % 100;
	
				
				if ((month > 0 && month < 13) && day != 0 ) {
					
					// then we have leap year
					if (present_year % 4 == 0) {
						
						if(day <= max_days_in_month[month-1]) {
						
							LocalDate date2 = LocalDate.of(present_year, month, day); 
							System.out.println(date2);
						}
	
					}else {
						if(day <= max_days_in_month[month-1] - 1) {
							
							LocalDate date2 = LocalDate.of(present_year, month, day); 
							System.out.println(date2);
						}			
					}					
				}
			
				
				
			}
			
			System.out.println("Palindromes. Years interval [" + from_year + ":" + to_year + ")");
			
	  } // function
  } // class

  
  public static void main(String[] args) {
	printBonusDatesBetween(2010, 2015);
	System.out.println("END");

  }
}

# -*- coding: utf-8 -*-
"""
Created on Tue Jun 23 20:53:09 2020

@author: Rokas
"""

def search(array, solution):
    
    array_lenght = len(array)
    index_mem = 0
    digit_mem = []
    
    for index, digit in enumerate(array[1:], start = 1):
        if (digit - index) >= 0:
            index_mem = index
            digit_mem = digit
    
    if index_mem != array_lenght - 1 and index_mem == 0:
        solution = []
    elif index_mem != array_lenght - 1 and index_mem != 0:
        solution.append(digit_mem)
        new_array = array[index_mem:]
        solution = search(new_array, solution)
    else:
        solution.append(digit_mem)

    return(solution)



def solving(array):
    
    solution = []
    
    array.reverse()
    
    if len(array) > 0:
        if array[0] == 0 and len(array) > 1:
            solution = search(array, solution)
            
    solution.reverse()

    return(solution)


def task_test():
    
    test_arrays = {
        0: [1, 3, 0, 2, 0, 1, 0],
        1: [1, -3, 0, 2, 0, 1, 0],
        2: [1, 9, 0, 2, 0, 1, 0],
        3: [1, 2, 0, 1, 0, 2, 0],
        4: [5],
        5: [0],
        6: [5, 4, 3, 2, 1, 0],
        7: [1, 2],
        8: [3, 3, 3, 3, 0],
        9: [-1, -1, 2, 0],
        10:[1, 2, 3, 4, 3, 2, 1, 0], 
        11:[2, -1, 3, 0, 0, 0],
        12:[1, 0],
        13:[2, -1, 3, -3, -1, 3, -1, -2, 1, 0]
        }
    
    for i in range(11):
        print(f'test{i},\ninput: {test_arrays[i]}, solution: {solving(test_arrays[i])}')



if __name__ == "__main__":
    
    import doctest
    import sys
    
    doctest.testmod()
    
    print(f'Input (eg: [3,0,2,-2,1,0]):')
    
    array = [i for i in eval(input())]
     
    print(f'input: {array}, solution: {solving(array)}')


      
  
      
'''
tests:
>> solving([1, 3, 0, 2, 0, 1, 0])
[1, 3, 2, 1]

>> solving([1, -3, 0, 2, 0, 1, 0])
[]

>> solving ([1, 9, 0, 2, 0, 1, 0])
[1,9]

>> solving([1, 2, 0, 1, 0, 2, 0])
[]

>> solving([5])
[]

>> solving([0])
[]

>> solving([5, 4, 3, 2, 1, 0])
[5]

>> solving([1, 2])
[]

>> solving([3, 3, 3, 3, 0])
[3, 3]

>> solving([-1, -1, 2, 0])
[]

>> solving([1, 2, 3, 4, 3, 2, 1, 0])
[1, 2, 4]

>> solving([2, -1, 3, 0, 0, 0])
[2, 3]

>> solving([1,0])
[1]

>> solving([2, -1, 3, -3, -1, 3, -1, -2, 1, 0])
[2, 3, 3, 1]

'''


